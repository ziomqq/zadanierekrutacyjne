package pl.gameXO.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class clsGame {
    //initialize board for game
    String[][] TicTacBoard = new String[3][3];
    boolean gamer;
    boolean win;
    int[] choice;
    int gameNO;

    public clsGame(){
        //prepare variables for game
        TicTacBoard[0] = new String[] {".",".","."};
        TicTacBoard[1] = new String[] {".",".","."};
        TicTacBoard[2] = new String[] {".",".","."};
        gamer = true;
        win = false;
        gameNO = 0;

    }

    //game
    //gamers have only 9 turns
    public void game(){
        while(this.gameNO < 9) {
            this.ShowTable(this.TicTacBoard, this.gamer, false);
            while(true){
                this.choice = this.getDataInput();
                if(this.choice[0] == -1 || this.choice[1] == -1) {
                    continue;
                }
                if(this.checkValid(this.choice, this.TicTacBoard)){ //if user chose correct position
                    this.setInput();
                    break;
                }
                else{
                    System.out.println("You can't do this. Select another field");
                }
            }
            this.win = this.checkStatusGame(this.TicTacBoard);
            if(this.win){
                break;
            }
            this.gameNO++; //increment turn
            this.gamer = !this.gamer; //change gamer
        }
    }


    //display table and prompt
    public void ShowTable(String[][] TicTacBoard, boolean gamer, boolean endGame){
        for (int i=0; i<30; i++){
            System.out.println();
        }
        for (String[] row : TicTacBoard){
            System.out.println(Arrays.toString(row).replace(",","|"));
        }

        if(!endGame){
            System.out.println("Enter coordinates");
            if (gamer) {
                System.out.println("Gamer X:");
            }
            else {
                System.out.println("Gamer O:");
            }
        }

    }

    //get data from user
    public int[] getDataInput(){
        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
        String answer = "";
        int[] correctArray = {0,0}; // first is row, second is column

        while(true) {
            try {
                /* This is protected code. If an problem occurs here, catch block is triggered */
                while ((answer = buffReader.readLine()) != null) {
                    break;
                }
            } catch (IOException e) {
                System.out.println("Error in line reader.");
            }
            if (answer != null) {
                answer = answer.replaceAll("\\s", "");
            }

            String[] ansArray = answer.split("");

            correctArray = checkCorrectInput(ansArray);
            if (correctArray[1] != -1){
               break;
            }
        }
        return correctArray;
    }
    //check if data input is correct
    public int[] checkCorrectInput(String [] ansArray){
        int[] correctArray = {0,0}; // first is row, second is column

        if(ansArray.length != 2){ //check if input has only 2 element
            correctArray[0] = -1;
        }
        else{
            for(int i =0; i<2; i++){
                if(checkNumber(ansArray[i])) { //if data is number
                    correctArray[1] = Integer.parseInt(ansArray[i])-1; //set column
                }
                else{
                    correctArray[0] = GetRows(ansArray[i]);
                }
            }
        }

        if(correctArray[0]<0 || correctArray[0]>2 || correctArray[1]<0 || correctArray[1]>2){ //if user give number different then 1-3 or letter different than A-C
            System.out.println("Incorrect input data. Try again");
            correctArray[0] = -1;
            correctArray[1] = -1;
        }
        return correctArray;
    }

    //check if string is numeric(integer)
    public boolean checkNumber(String str){
        boolean isValid = false;

        try{
            Integer.parseInt(str);
            isValid = true;
        }
        catch(NumberFormatException e){}
        return isValid;
    }
    //translate row to number

    public int GetRows(String row){
        Map<String, Integer> dictonary = new HashMap<String, Integer>() ;

        dictonary.put("a",0);
        dictonary.put("b",1);
        dictonary.put("c",2);

        return dictonary.getOrDefault(row.toLowerCase(), -1);
    }
    //check if user can set this field in table
    public boolean checkValid(int[] choice, String[][] TicTacBoard){
        boolean result = false;
        if(TicTacBoard[choice[0]][choice[1]].contains(".")){
            result = true;
        }
        return result;
    }
    //check status of the game
    public boolean checkStatusGame(String[][] TicTacBoard){

        for(int i=0; i<3; i++){
            //check vertical
            if(TicTacBoard[0][i].contains(TicTacBoard[1][i]) && TicTacBoard[0][i].contains(TicTacBoard[2][i]) && !TicTacBoard[0][i].contains(".")){
                return true;
            }
            //check horizontal
            if(TicTacBoard[i][0].contains(TicTacBoard[i][1]) && TicTacBoard[i][0].contains(TicTacBoard[i][2]) && !TicTacBoard[i][0].contains(".")){
                return true;
            }
        }
        //check diagonal
        if(TicTacBoard[0][0].contains(TicTacBoard[1][1]) && TicTacBoard[0][0].contains(TicTacBoard[2][2]) && !TicTacBoard[0][0].contains(".")){
            return true;
        }
        if(TicTacBoard[0][2].contains(TicTacBoard[1][1]) && TicTacBoard[0][2].contains(TicTacBoard[2][0]) && !TicTacBoard[0][2].contains(".")){
            return true;
        }

        return false;
    }

    //set user select
    public void setInput(){
        String gamer = (this.gamer) ? "X":"O";
        this.TicTacBoard[this.choice[0]][this.choice[1]] = gamer;
    }
}
