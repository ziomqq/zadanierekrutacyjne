package pl.gameXO.app;

public class clsMain {

    public static void main(String[] args) {

        clsGame gameTTT = new clsGame();

        System.out.println("Tic Tac Toe!");
        System.out.println("Tips: A-C rows, 1-3 columns");
        System.out.println("Press enter to start game...");
        new java.util.Scanner(System.in).nextLine();

        gameTTT.game();

        gameTTT.ShowTable(gameTTT.TicTacBoard, gameTTT.gamer, true);
        if(gameTTT.win){
            String signWinner = (gameTTT.gamer) ? "X":"O";
            System.out.println("The winner is: " + signWinner);
        }
        else{
            System.out.println("Result of a game ending in a tie.");
        }
    }
}
