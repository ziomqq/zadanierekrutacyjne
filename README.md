Instrukcja:

Aplikacja napisana z wykorzystaniem środowiska IntelliJ Idea w języku JAVA. Jeśli wykorzystywane jest to samo środowisko wystarczy otworzyć projekt w tym środowisku i uruchomić go z poziomu clsMain.
Jesli wykorzystywane jest inne środowisko prosze o utworzenie tymczasowego projektu, w którym utworzy się pakiet pl.gameXO.app i do niego zaimportuje pliki: clsMain.java, clsGame.java oraz clsGameTest.java.
Podobnie jak wczesniej, program uruchamiany jest z poziomu clsMain.
Testy jednostkowe znajdują się w pliku clsGameTest