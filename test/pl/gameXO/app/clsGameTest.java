package pl.gameXO.app;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class clsGameTest {
    private clsGame game;
    private String[][] TicTacBoard = new String[3][3];
    @Before
    public void setUp(){
        game = new clsGame();
        TicTacBoard[0] = new String[] {".",".","."};
        TicTacBoard[1] = new String[] {".",".","."};
        TicTacBoard[2] = new String[] {".",".","."};
    }

    @Test
    public void checkTranslation(){
        assertEquals(0,game.GetRows("A"));
        assertEquals(1,game.GetRows("B"));
        assertEquals(2,game.GetRows("C"));
        assertEquals(0,game.GetRows("a"));
        assertEquals(1,game.GetRows("b"));
        assertEquals(2,game.GetRows("c"));
        assertEquals(-1,game.GetRows("AAAAA"));
        assertEquals(-1,game.GetRows("1"));
    }
    @Test
    public void checkResultOfGame(){
        assertFalse(game.checkStatusGame(TicTacBoard));
        TicTacBoard[1] = new String[] {"O","O","O"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[1] = new String[] {".",".","."};
        TicTacBoard[2] = new String[] {"O","O","O"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[2] = new String[] {".",".","."};
        TicTacBoard[0] = new String[] {"O","O","O"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {"X","X","X"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {".",".","."};
        TicTacBoard[1] = new String[] {"X","X","X"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[1] = new String[] {".",".","."};
        TicTacBoard[2] = new String[] {"X","X","X"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {"X",".","."};
        TicTacBoard[1] = new String[] {".","X","."};
        TicTacBoard[2] = new String[] {".",".","X"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {"O",".","."};
        TicTacBoard[1] = new String[] {".","O","."};
        TicTacBoard[2] = new String[] {".",".","O"};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {".",".","O"};
        TicTacBoard[1] = new String[] {".","O","."};
        TicTacBoard[2] = new String[] {"O",".","."};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {".",".","X"};
        TicTacBoard[1] = new String[] {".","X","."};
        TicTacBoard[2] = new String[] {"X",".","."};
        assertTrue(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {".",".","X"};
        TicTacBoard[1] = new String[] {".","X","X"};
        TicTacBoard[2] = new String[] {"O",".","."};
        assertFalse(game.checkStatusGame(TicTacBoard));
        TicTacBoard[0] = new String[] {"O",".","O"};
        TicTacBoard[1] = new String[] {".","X","."};
        TicTacBoard[2] = new String[] {"O",".","O"};
        assertFalse(game.checkStatusGame(TicTacBoard));
    }

    @Test
    public void checkCorrectInput(){
        int[] badResult = {-1,-1};
        assertArrayEquals(badResult, game.checkCorrectInput(new String[]{"A","2","7"}));
        assertArrayEquals(badResult, game.checkCorrectInput(new String[]{"A","8"}));
        assertArrayEquals(badResult, game.checkCorrectInput(new String[]{"A"}));
        assertArrayEquals(badResult, game.checkCorrectInput(new String[]{"1"}));
        assertArrayEquals(badResult, game.checkCorrectInput(new String[]{"F","1"}));
        assertArrayEquals(new int[]{0,0}, game.checkCorrectInput(new String[]{"A","1"}));
        assertArrayEquals(new int[]{0,1}, game.checkCorrectInput(new String[]{"A","2"}));
        assertArrayEquals(new int[]{0,2}, game.checkCorrectInput(new String[]{"A","3"}));
        assertArrayEquals(new int[]{1,0}, game.checkCorrectInput(new String[]{"B","1"}));
        assertArrayEquals(new int[]{1,1}, game.checkCorrectInput(new String[]{"B","2"}));
        assertArrayEquals(new int[]{1,2}, game.checkCorrectInput(new String[]{"B","3"}));
        assertArrayEquals(new int[]{2,0}, game.checkCorrectInput(new String[]{"C","1"}));
        assertArrayEquals(new int[]{2,1}, game.checkCorrectInput(new String[]{"C","2"}));
        assertArrayEquals(new int[]{2,2}, game.checkCorrectInput(new String[]{"C","3"}));

    }

    @Test
    public void checkConvertingStringToNumeric(){
        assertFalse(game.checkNumber("A"));
        assertFalse(game.checkNumber("s"));
        assertFalse(game.checkNumber("."));
        assertFalse(game.checkNumber("!"));
        assertTrue(game.checkNumber("1"));
        assertTrue(game.checkNumber("0"));
    }

    @Test
    public void checkPosibilityToSetField(){
        TicTacBoard[0] = new String[] {"O",".","O"};
        TicTacBoard[1] = new String[] {".","X","."};
        TicTacBoard[2] = new String[] {"O",".","X"};
        assertFalse(game.checkValid(new int[]{0,0}, TicTacBoard));
        assertFalse(game.checkValid(new int[]{2,2}, TicTacBoard));
        assertFalse(game.checkValid(new int[]{1,1}, TicTacBoard));
        assertTrue(game.checkValid(new int[]{0,1}, TicTacBoard));
        assertTrue(game.checkValid(new int[]{2,1}, TicTacBoard));
    }

}